# Red De Sensores Ambientales #

Este proyecto consiste en una red de sensores distribuidos en una sala que se comunican con un nodo central para proporcionarle información ambiental respecto a los siguientes parámetros:

* Temperatura.
* Presión.
* Humedad relativa.
* Luminosidad.
* Magnetómetro.
* Acelerómetro.
* Giróscopo.
* Otros sensores adicionales (Para los cuáles se crea un framework para la integración rápida de librerías).

Se dotará al sistema de las siguientes características:

* Técnicas de bajo consumo en el microcontrolador.
* Detección automática de los sensores conectados al bus (De aquellos que sea posible disponer de dicha característica).
* Comunicación mediante un transceptor nRF24L01, destinado las comunicaciones de bajo consumo.
